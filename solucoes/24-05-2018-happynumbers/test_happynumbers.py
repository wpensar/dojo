import unittest
from happynumbers import happynumbers
from happynumbers import soma_dos_quadrados_dos_digitos

class TestHappynumbers(unittest.TestCase):

    def test_sadnumber_0(self):
        self.assertEqual(happynumbers(0), False)

    def test_happynumber_1(self):
        self.assertEqual(happynumbers(1), True)

    def test_happynumber_7(self):
        self.assertEqual(happynumbers(7), True)
    def test_happynumber_49(self):
        self.assertEqual(happynumbers(49), True)
    
    def test_sadnumber_2(self):
        self.assertEqual(happynumbers(2), False)

    def test_sadnumber_3(self):
        self.assertEqual(happynumbers(3), False)

    def test_soma_dos_quadrados(self):
        self.assertEqual(soma_dos_quadrados_dos_digitos(5), 25)
        self.assertEqual(soma_dos_quadrados_dos_digitos(4), 16)
        self.assertEqual(soma_dos_quadrados_dos_digitos(11), 2)
        self.assertEqual(soma_dos_quadrados_dos_digitos(81), 65)

unittest.main()
        
