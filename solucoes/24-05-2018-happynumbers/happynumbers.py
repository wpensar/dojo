def soma_dos_quadrados_dos_digitos(number):
    return sum(
        [
            int(i)**2 for i in str(number)
        ]
    )
    
def happynumbers(number):
    numeros_passados = set()

    while number != 1 and number not in numeros_passados:
        numeros_passados.add(number)
        number = soma_dos_quadrados_dos_digitos(number)
    
    return number == 1