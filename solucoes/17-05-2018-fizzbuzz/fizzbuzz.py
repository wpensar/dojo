def fizzbuzz(numero):
	retorno = numero

	if(numero % 15 == 0):
		retorno = "fizzbuzz"
	elif(numero % 5 == 0):
		retorno = "buzz"
	elif(numero % 3 == 0):
		retorno = "fizz"
		
	return retorno
