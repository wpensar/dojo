import unittest
from fizzbuzz import fizzbuzz

class TestFizzbuzz(unittest.TestCase):

    def test_3_fizz(self):
        self.assertEqual(fizzbuzz(3), "fizz")

    def test_6_fizz(self):
        self.assertEqual(fizzbuzz(6), "fizz")

    def test_5_buzz(self):
        self.assertEqual(fizzbuzz(5), "buzz")

    def test_10_buzz(self):
        self.assertEqual(fizzbuzz(10), "buzz")

    def test_n_should_be_n(self):
        self.assertEqual(fizzbuzz(7), 7)

    def test_15_fizzbuzz(self):
        self.assertEqual(fizzbuzz(15), "fizzbuzz")

    def test_30_fizzbuzz(self):
        self.assertEqual(fizzbuzz(30), "fizzbuzz")

    def test_n_fizzbuzz(self):
        resultado = [
            1,
            2,
            'fizz',
            4,
            'buzz',
            'fizz',
            7,
            8,
            'fizz',
            'buzz',
            11,
            'fizz',
            13,
            14,
            'fizzbuzz'
        ] 

        resultado_calculado = [
            fizzbuzz(numero) 
            for numero in xrange (1,16)
        ]

        self.assertEqual(resultado, resultado_calculado)

unittest.main()
        
